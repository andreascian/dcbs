#
# Docker image to build DIVELK 3.0.0
#
FROM ubuntu:16.04

MAINTAINER DAVE Embedded Systems (devel@dave.eu)

# install required build packages
RUN apt-get update && \
	apt-get install -y build-essential git-core gcc-arm-none-eabi \
		flex bison vim curl libc6-dev-i386 bc ccache lzop \
		sudo lintian && \
	apt-get install -y u-boot-tools && \
	apt-get install -y wget && \
	echo "dash dash/sh boolean false" | debconf-set-selections && \
	DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# get linaro toolchain
RUN wget --no-verbose https://releases.linaro.org/components/toolchain/binaries/5.3-2016.02/arm-linux-gnueabihf/gcc-linaro-5.3-2016.02-x86_64_arm-linux-gnueabihf.tar.xz  && \
	mkdir /toolchain && \
	tar -Jxvf gcc-linaro-5.3-2016.02-x86_64_arm-linux-gnueabihf.tar.xz -C /toolchain && \
	rm gcc-linaro-5.3-2016.02-x86_64_arm-linux-gnueabihf.tar.xz
COPY env.sh /toolchain/env.sh

RUN mkdir /build
WORKDIR /build

COPY docker_entrypoint.sh /root/docker_entrypoint.sh
ENTRYPOINT ["/root/docker_entrypoint.sh"]
